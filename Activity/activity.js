// INSERT ONE
db.hotel.insert({
    name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
    });

//INSERT MANY

db.hotel.insertMany([
 {
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit a small family going on a vacation",
    rooms_available: 15,
    isAvailable: false
    },
    {
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A simple room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false
    }
    ]);

// FIND NAME DOUBLE
db.hotel.find({name: "double"});

//UPDATE QUEEN TO 0 AVAIBLE ROOM
db.hotel.updateOne(
    {
        name: "queen"
    },
    {
        $set: {
            room_available: 0
        }
    }
);

//DELETE MANY OF ALL 0 AVAILABLE ROOM
db.hotel.deleteMany({
    room_available: 0
    });















