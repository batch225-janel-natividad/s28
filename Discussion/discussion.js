
//MONGO DB OPERATIONS
// FOR CREATING OR INSERTING DATA INTO DATABASE
db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
       },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
    })
//INSERT MANY
db.users.insertMany([
    {
        firstName: "Chito",
        lastName: "Miranda",
        age: 43,
        contact: {
            phone: "5347786",
            email: "chito@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Francis",
        lastName: "Magalona",
        age: 61,
        contact: {
            phone: "5347786",
            email: "francisM@email.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
])
//============================================================================

//FOR QUERYING ALL THE DATA IN DATABASE
//FOR FINDING DOCUMENTS
db.users.find()


//=============================================================
//FIND ONE
db.users.find({firstName: "Francis", age: 61});


//====================================================================
//DELETE ONE ITEM IN THE COLLECTION
db.users.deleteOne({
    firstName: "Ely"
    });

//DELETE MANY
db.users.deleteMany({
    department: "none"
    });

//=======================================================================
//UPDATE ONE
db.users.updateOne(
    {
        firstName: "Chito"
    },
    {
        $set: {
            lastName: "Esguerra"
        }
    }
);

//UPDATE TO MANY
db.users.updateMany(
    {
        department: "DOH"
    },
    {
        $set: {
            department: "HR"
        }
    }
    );